# Kioptrix 5
- [Kioptrix 5 - SalmonSec](https://salmonsec.com/blogs/kioptrix_level_5) - [@Salmonsec](https://twitter.com/salmonsec))
- [VulnHub – Kioptrix: Level 1.4 Walkthrough - Steflan's Security Blog](https://steflan-security.com/vulnhub-kioptrix-1-4-walkthrough/) - [@Stefano Lanaro](https://www.linkedin.com/in/stefano-lanaro-799727182/)
