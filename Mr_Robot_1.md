# Mr Robot 1
- [Mr Robot 1 - SalmonSec](https://salmonsec.com/blogs/mr_robot_1) - [@Salmonsec](https://twitter.com/salmonsec)
- [Mr Robot WalkThough on Hack's Eyes in French](https://www.youtube.com/watch?v=GDwlUnJSwos) - [@tenflo1](https://twitter.com/tenflo1)
- [Mr Robot WalkThrough for complete beginners](https://github.com/Shadow-Admins/Cyber_Club/tree/main/Starting_Point/VulnHub/MrRobot) - [@#$h@d0w_@dm1n$](https://github.com/Shadow-Admins)
