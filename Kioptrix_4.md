# Kioptrix 4
- [Kioptrix 4 - SalmonSec](https://salmonsec.com/blogs/kioptrix_level_4) - [@Salmonsec](https://twitter.com/salmonsec))
- [VulnHub – Kioptrix: Level 1.3 Walkthrough - Steflan's Security Blog](https://steflan-security.com/vulnhub-kioptrix-level-1-3-walkthrough/) - [@Stefano Lanaro](https://www.linkedin.com/in/stefano-lanaro-799727182/)
