# Kioptrix 1
- [Kioptrix 1 - SalmonSec](https://salmonsec.com/blogs/kioptrix_level_1) - [@Salmonsec](https://twitter.com/salmonsec))

- [Kioptrix: Level 1 Walkthrough](https://rafaelmedeiros94.medium.com/kioptrix-level-1-walkthrough-9db529ba2888) - (https://www.linkedin.com/in/rafael-de-jesus-medeiros/ )
- [VulnHub – Kioptrix: Level 1 Walkthrough - Steflan's Security Blog](https://steflan-security.com/vulnhub-kioptrix-level-1-walkthrough/) - [@Stefano Lanaro](https://www.linkedin.com/in/stefano-lanaro-799727182/)

