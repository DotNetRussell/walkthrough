# Kioptrix 3
- [Kioptrix 3 - SalmonSec](https://salmonsec.com/blogs/kioptrix_level_3) - [@Salmonsec](https://twitter.com/salmonsec))
- [KIOPTRIX: LEVEL 1.2 Vulnhub Walkthrough](https://iamnasef.com/walkthrough/kl12/) - [@iamnasef](https://www.linkedin.com/in/iamnasef/)
- [VulnHub – Kioptrix: Level 1.2 Walkthrough - Steflan's Security Blog](https://steflan-security.com/vulnhub-kioptrix-level-1-2-walkthrough/) - [@Stefano Lanaro](https://www.linkedin.com/in/stefano-lanaro-799727182/)
